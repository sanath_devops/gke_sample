terraform {
  backend "gcs" {
    credentials = "../../terraform-project-325918-5c3af3130eda.json"
    bucket      = "tf-state-gke-sanath"
    prefix      = "terraform-dev/state/tf.state"
  }
}

provider "google" {
  credentials = file(var.gcp_auth_file)
  project = var.project_name
  region  = var.region_name
  zone    = var.zone_name
}

module "network" {
  source = "git::ssh://git@bitbucket.org/sanath_devops/gke_modules.git//network?ref=master"
  #source = "../modules/vpc-default"

  network_name                 = "${var.project_name}-${var.environment_name}-${var.network_name}"
  subnetwork_name              = var.subnetwork_name
  region_name                  = var.region_name
  subnetwork_flow_logs_enabled = var.subnetwork_flow_logs_enabled

  // subnetwork primary and secondary CIDRS for IP aliasing
  subnetwork_range    = var.subnetwork_range
  subnetwork_pods     = var.subnetwork_pods
  subnetwork_services = var.subnetwork_services
}

module "cluster" {
  source = "git::ssh://git@bitbucket.org/sanath_devops/gke_modules.git//cluster?ref=master"

  cluster_name                     = "${var.project_name}-${var.environment_name}-cluster"
  region_name                      = var.region_name
  project_name                     = var.project_name
  kubernetes_version               = var.kubernetes_version
  network_name                     = "${var.project_name}-${var.environment_name}-${var.network_name}"
  nodes_subnetwork_name            = module.network.subnetwork
  pods_secondary_ip_range_name     = module.network.gke_pods_1
  services_secondary_ip_range_name = module.network.gke_services_1
  # private cluster options
  enable_private_endpoint = false
  enable_private_nodes    = true
  master_ipv4_cidr_block  = var.master_ipv4_cidr_block

  master_authorized_network_cidrs = [
    {
      # This is the module default, but demonstrates specifying this input.
      cidr_block   = "0.0.0.0/0"
      display_name = "from the Internet"
    },
  ]

  depends_on = [module.network]
}

module "node_pool" {
  source = "git::ssh://git@bitbucket.org/sanath_devops/gke_modules.git//node_pool?ref=master"

  node_pool_name     = "${var.project_name}-${var.environment_name}"
  region_name        = module.cluster.region
  gke_cluster_name   = module.cluster.name
  machine_type       = var.machine_type
  min_node_count     = var.min_node_count
  max_node_count     = var.max_node_count
  kubernetes_version = module.cluster.kubernetes_version
  depends_on         = [module.cluster]
}


# google_client_config and kubernetes provider must be explicitly specified like the following.
data "google_client_config" "default" {}

# provider "kubernetes" {
#   host                   = "https://${module.cluster.endpoint}"
#   token                  = data.google_client_config.default.access_token
#   cluster_ca_certificate = base64decode(module.cluster.ca_certificate)
# }

