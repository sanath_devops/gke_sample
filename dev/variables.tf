variable "project_name" {
}

variable "gcp_auth_file" {
  default = "../../terraform-project-325918-2c570f0fc8f5.json"

}

variable "region_name" {
}

variable "zone_name" {
}

variable "tf_state_bucket_name" {
  default = "tf-state-gke-sanath"
}

variable "environment_name" {
}

variable "service_account" {
}

variable "network_name" {
  description = "the name of the network"
}

variable "subnetwork_name" {
  description = "name for the subnetwork"
}

variable "subnetwork_range" {
  description = "CIDR for subnetwork nodes"
}

variable "subnetwork_pods" {
  description = "secondary CIDR for pods"
}

variable "subnetwork_services" {
  description = "secondary CIDR for services"
}

variable "kubernetes_version" {
  description = "The minimum version of master nodes. This can be changed to upgrade the cluster - remember to upgrade the Kubernetes version for node pools (managed separately)."
}

variable "master_ipv4_cidr_block" {
  description = "The /28 range for the master instances. Must be set if enable_private_nodes or enable_private_endpoint is true"
  default     = null
}

variable "subnetwork_flow_logs_enabled" {
  description = "If you want to set up flow logs you will need to set this to enabled and update subnetwork_flow_logs variable defaults if necessary."
  default     = false
}

variable "min_node_count" {
  description = "Minimum number of nodes for autoscaling, per availability zone."
}

variable "max_node_count" {
  description = "Maximum number of nodes for autoscaling, per availability zone."
}

variable "machine_type" {
  description = "The machine type of nodes in the pool."
  default     = "n1-standard-4"
}